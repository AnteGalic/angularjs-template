"use strict";

app.config(["$ocLazyLoadProvider", function ($ocLazyLoadProvider) {
	$ocLazyLoadProvider.config({
		debug: false,
		events: false,
		modules: [
			{
				name: "lazy-home",
				files: [
					"app/controller/home-ctrl.js"
				]
			},
			{
				name: "lazy-test",
				files: [
					"app/controller/test-ctrl.js"
				]
			}
		]
	})
}
]);
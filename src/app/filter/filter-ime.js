"use strict";

app.filter("filterIme", [function () {

    return function (items, code) {
        var filtered = [];
        if (code) {

            for (var i = 0; i < items.length; i++) {
                var item = items[i];
                var itemSplit = item.naziv.split(" ")
                if (itemSplit[1].charAt(0).toUpperCase() == code.toUpperCase()) {
                    filtered.push(item);
                }
                else if (itemSplit[1].substring(0, 2).toUpperCase() == code.toUpperCase()) {
                    filtered.push(item)
                }
            }
            return filtered;
        }

    }
}]);
var app = angular.module("MyApp", [
	"ngAnimate",
	"ngResource",
	"ui.router",
	"ui.bootstrap",
	"oc.lazyLoad",
	"angularUtils.directives.dirPagination"
]);

app.run([
	"$rootScope", "$state", "$location",
	function ($rootScope, $state, $location) {

	}
])

app.config(["$stateProvider", "$urlRouterProvider",
	function ($stateProvider, $urlRouterProvider) {
		$stateProvider
			.state("app", {
				abstract: true,
				url: "/app",
				templateUrl: "template/app.html",
			})
			.state("app.home", {
				url: "/home",
				templateUrl: "view/home.html",
				resolve: {
					deps: ["$ocLazyLoad", function ($ocLazyLoad) {
						return $ocLazyLoad.load([
							"lazy-home"
						], { serie: true }).then(function () { });
					}],
				}
			})
			.state("app.test", {
				url: "/test",
				templateUrl: "view/test.html",
				resolve: {
					deps: ["$ocLazyLoad", function ($ocLazyLoad) {
						return $ocLazyLoad.load([
							"lazy-test"
						], { serie: true }).then(function () { });
					}],
				}
			})

		

		$urlRouterProvider.otherwise("/app/home");

	}
])